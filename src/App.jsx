import 'bootstrap/dist/css/bootstrap.min.css';

import { useEffect, useState } from "react"

import {Buscador} from './Components/Buscador';
import {EmojiList} from './Components/Emojilist'
import Pagination from "react-js-pagination";
import {TituloEncabezado} from './Components/Titulo';
import datos from './database/emoji-list.json';

let LIMIT=24;
let URL = `http://localhost:3001/emojis?_limit=${LIMIT}`;

function App() {
  let [emojis, setEmojis] = useState([]);
  
  let [busqueda, setBusqueda] = useState('');
  let [pagina, setPagina] = useState(1)

  let emojiFilter= emojis.filter((emoji)=> {
    let emojiTitle=emoji.title.toLowerCase();
    let busquedaLower=busqueda.toLowerCase();

    if(emojiTitle.includes(busquedaLower)){
      return emoji

    }
  }
  )

  useEffect(() => {

    fetch(`${URL}&_page=${pagina}`)
    .then(res => res.json()).then(datos => {
      setEmojis(datos);
    })
  },[pagina])

  function actualizarInput(evento){
    let entradaTeclado = evento.target.value;
    setBusqueda(entradaTeclado);
  }
  
  function handlePageChange(nuevaPagina) {
    setPagina(nuevaPagina);
  }

  return (
    <div className="container">
      <TituloEncabezado />
      <Buscador valorInput = {busqueda} onInputChange = {actualizarInput} />
      
      <EmojiList datos={ busqueda ? emojiFilter : emojis} /> 

      <div className='d-flex justify-content-center'>
      <Pagination
          itemClass="page-item"
          linkClass="page-link"
          activePage={pagina}
          itemsCountPerPage={LIMIT}
          totalItemsCount={1820}
          onChange={handlePageChange}
        />
      </div>
        
    </div>
  )
}

export default App
