import { EmojiItem } from './Emojiitem';

export  function EmojiList({datos}) {
    let renderEmoji = datos.map((emoji) => {
        return (
            <div className="col-3" key={emoji.title}>
                <EmojiItem 
                title={emoji.title} 
                symbol={emoji.symbol} 
                keywords={emoji.keywords} />
            </div>
        )
    })
    return(
        <div className="row py-5">
            {renderEmoji}
        </div>
    );
}