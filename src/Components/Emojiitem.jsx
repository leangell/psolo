export  function EmojiItem({title,symbol,keywords }) {
    return(
        <div className="card">
            <h5 className="card-header" text-center >{title}</h5>
            <div className="card-body">
                <h1 className="text-center">{symbol}</h1>
                <p className="card-text">{keywords}</p>
            </div>
        </div>
    )
}